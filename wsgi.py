import os

from api.app import create_app
from config import config

app = create_app(config_module=config[os.environ.get('FLASK_CONFIG') or 'default'])

if __name__ == '__main__':
    app.run()
