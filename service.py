from config import config
from api.models import db, Role, User
from api.app import create_app
from flask_migrate import Migrate, upgrade
import click
import sys
import os

COV = None
if os.environ.get('FLASK_COVERAGE'):
    import coverage
    COV = coverage.coverage(branch=True, include='app/*')
    COV.start()


app = create_app(config[os.environ.get('FLASK_CONFIG') or 'default'])
migrate = Migrate(app, db)
@app.shell_context_processor
def make_shell_context():
    return dict(db=db)


@app.cli.command()
@click.option('--coverage/--no-coverage', default=False,
              help='Run tests under code coverage.')
def test(coverage):
    """Run the unit tests."""
    if coverage and not os.environ.get('FLASK_COVERAGE'):
        import subprocess
        os.environ['FLASK_COVERAGE'] = '1'
        sys.exit(subprocess.call(sys.argv))

    import unittest
    tests = unittest.TestLoader().discover('tests')
    unittest.TextTestRunner(verbosity=2).run(tests)
    if COV:
        COV.stop()
        COV.save()
        print('Coverage Summary:')
        COV.report()
        basedir = os.path.abspath(os.path.dirname(__file__))
        covdir = os.path.join(basedir, 'tmp/coverage')
        COV.html_report(directory=covdir)
        print('HTML version: file://%s/index.html' % covdir)
        COV.erase()


@app.cli.command()
@click.option('--length', default=25,
              help='Number of functions to include in the profiler report.')
@click.option('--profile-dir', default=None,
              help='Directory where profiler data files are saved.')
def profile(length, profile_dir):
    """Start the application under the code profiler."""
    #from werkzeug.contrib.profiler import ProfilerMiddleware
    from werkzeug.middleware.profiler import ProfilerMiddleware
    app.wsgi_app = ProfilerMiddleware(app.wsgi_app, restrictions=[length],
                                      profile_dir=profile_dir)
    app.run()


@app.cli.command('ready')
def deploy():
    """Run deployment tasks."""
    # to run
    # export FLASK_APP=adfinem.py
    # flask ready <- execute
    # migrate database to latest revision
    upgrade()

    # create or update user roles
    Role.insert_roles()

    # create users
    u = User(username="amit@ashah.de", password="Dragon123",
             confirmed=True, name="Amit")
    u1 = User(username="info@ashah.de", password="Dragon123",
              confirmed=True, name="Admin Amit")
    u1.role = Role.query.filter_by(name="Administrator").first()
    db.session.add_all(([u, u1]))
    db.session.commit()
