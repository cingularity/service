import os
import redis

basedir = os.path.abspath(os.path.dirname(__file__))


TESTING = True
SECRET_KEY = 'secret'
SQLALCHEMY_DATABASE_URI = 'postgresql+psycopg2://adfinem:AdFinem123@localhost/adfinemtest'
USE_TOKEN_AUTH = True
USE_RATE_LIMITS = False
SQLALCHEMY_TRACK_MODIFICATIONS = False
# the site URL
SITE_URL = 'http://einzel.ashah.de/'
SITE_ADMIN = 'info@ashah.de'  # this user automatically becomes the site admin

# mail
#MAIL_SERVER = os.environ.get('MAIL_SERVER', 'mail1.pwhost.de')
MAIL_SERVER = 'mail1.pwhost.de'
#MAIL_PORT = int(os.environ.get('MAIL_PORT', '587'))
MAIL_PORT = 587
# MAIL_USE_TLS = os.environ.get('MAIL_USE_TLS', 'true').lower() in \
#    ['true', 'on', '1']
MAIL_USE_TLS = 'true'
MAIL_USERNAME = os.environ.get('MAIL_USERNAME') or 'info@ashah.de'
MAIL_PASSWORD = os.environ.get('MAIL_PASSWORD') or 'Dragon1234'
MAIL_SUBJECT_PREFIX = '[AdFinem]'
MAIL_SENDER = 'AdFinem Admin <info@ashah.de>'

SITE_LIST = [
    "http://localhost:3000",
    "http://192.168.178.35:3000",
    "http://localhost:3001",
    "https://adfinem.datenium.com",
    "https://www.adfinem.datenium.com",
    "https://serbot.de",
    "https://www.serbot.de",
    "https://adfinemadmin.datenium.com",
    "https://www.adfinemadmin.datenium.com",
]
