from threading import Thread
from flask import current_app, render_template
from flask_mail import Message, Mail
from .translations.translate import translate


mail = Mail()

def send_async_email(app, msg):
    with app.app_context():
        mail.send(msg)


def send_email(to, subject, template, language='en', **kwargs):
    app = current_app._get_current_object()
    link = app.config['SITE_URL']
    msg = Message(app.config['MAIL_SUBJECT_PREFIX'] + ' ' + translate(subject, language),
                  sender=app.config['MAIL_SENDER'], recipients=[to])
    msg.body = render_template(template + '-' + language + '.txt', link=link, **kwargs)
    msg.html = render_template(template + '-' + language + '.html',link=link, **kwargs)
    thr = Thread(target=send_async_email, args=[app, msg])
    thr.start()
    return thr

def send_contact_form(to, subject, template, **kwargs):
    app = current_app._get_current_object()
    link = app.config['SITE_URL']
    msg = Message(
        subject=subject,
        sender=app.config['MAIL_SENDER'], 
        recipients=[to]
    )
    msg.body = render_template(template + '.txt', link=link, **kwargs)
    msg.html = render_template(template + '.html',link=link, **kwargs)
    thr = Thread(target=send_async_email, args=[app, msg])
    thr.start()
    return thr