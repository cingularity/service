import re
from flask import current_app
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.exc import IntegrityError


### change comma to point ###
def comma2Point(price):
    '''Will convert only values that seem like prices'''
    decmark_reg = re.compile('(?<=\d),(?=\d)')
    return decmark_reg.sub('.', price)


# extracts hash tags from a description
def extract_hash_tags(text):
    return list(
        set([re.sub(r"#+", "#", k)
             for k in set(
            [re.sub(r"(\W+)$", "", j, flags=re.UNICODE)
             for j in set([i for i in text.split() if i.startswith("#")])
             ]
        )
        ])
    )



# checks to see if the entry already exists, if yes return the object, else creates one and returns it
def get_one_or_create(session, model, create_method='', create_method_kwargs=None, **kwargs):
    try:
        return session.query(model).filter_by(**kwargs).one(), True
    except NoResultFound:
        kwargs.update(create_method_kwargs or {})
        created = getattr(model, create_method, model)(**kwargs)
        try:
            session.add(created)
            session.flush()
            # with session.begin_nested():
            #created = getattr(model, create_method, model)(**kwargs)
            # session.add(created)
            return created, False
        except IntegrityError:
            return session.query(model).filter_by(**kwargs).one(), True
