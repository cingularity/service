from flask import jsonify, url_for, current_app


class ValidationError(ValueError):
    pass


def not_modified():
    response = jsonify({'status': 304, 'error': 'not_modified'})
    response.status_code = 304
    return response


def bad_request(message):
    response = jsonify({'status': 400, 'error': 'bad_request',
                        'message': message})
    response.status_code = 400
    return response


def unauthorized(message=None):
    if message is None:
        if current_app.config['USE_TOKEN_AUTH']:
            message = 'Please authenticate with your token.'
        else:
            message = 'Please authenticate.'
    response = jsonify({'status': 401, 'error': 'unauthorized',
                        'message': message})
    response.status_code = 401
    if current_app.config['USE_TOKEN_AUTH']:
        response.headers['Location'] = url_for('token.request_token')
    return response


def not_found(message):
    response = jsonify({'status': 404, 'error': 'not found',
                        'message': message})
    response.status_code = 404
    return response


def file_too_large(message):
    response = jsonify({'status': 413, 
                        'error': 'Request Entity Too Large',
                        'message': message})
    response.satus_code = 413
    return response

def not_allowed():
    response = jsonify({'status': 405, 'error': 'method not allowed'})
    response.status_code = 405
    return response


def precondition_failed():
    response = jsonify({'status': 412, 'error': 'precondition failed'})
    response.status_code = 412
    return response


def too_many_requests(message='exceeded_request_rate'):
    response = jsonify({'status': 429, 'error': 'too_many_requests',
                        'message': message})
    response.status_code = 429
    return response


def permission_denied(message="not_required_permission"):
    response = jsonify({'status': 403, 'error': 'no_permission',
                        'message': message})
    response.status_code = 403
    return response


def not_acceptable(message='request_not_acceptable'):
    response = jsonify({
        'status': 406, 'error': 'not_acceptable',
        'message': message,
    })
    response.status_code=406
    return response