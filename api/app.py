import os
from flask import Flask, request
from flask_migrate import Migrate
from flask_uuid import FlaskUUID
from flask_compress import Compress
import logging
from .models import db
from .email import mail
from .auth import auth
from .decorators import json, etag
from .errors import not_found, not_allowed


try:
    from flask_cors import CORS
except ImportError:
    # Path hack allows examples to be run without installation.
    import os
    parentdir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    os.sys.path.insert(0, parentdir)

    from flask_cors import CORS

migrate = Migrate()
flask_uuid = FlaskUUID()

def create_app(config_module=None):
    app = Flask(__name__)
    app.config.from_object(config_module or
                           os.environ.get('FLASK_CONFIG') or
                           'config')
    Compress(app)

    db.init_app(app)
    CORS(app)
    # CORS(
    #     app, 
    #     resources={r'*': {"origins": app.config['SITE_LIST']}},
    #     expose_headers='Location'
    # )

    flask_uuid.init_app(app)
    mail.init_app(app)
    migrate.init_app(app, db)

    import logging
    logging.basicConfig(filename=app.config['LOGFILE'], level=logging.ERROR)

    from api.v1 import api as api_blueprint
    app.register_blueprint(api_blueprint, url_prefix='/v1')

    if app.config['USE_TOKEN_AUTH']:
        from api.token import token as token_blueprint
        app.register_blueprint(token_blueprint, url_prefix='/auth')

        from api.public import public as public_blueprint
        app.register_blueprint(public_blueprint, url_prefix='/public')


    @app.route('/')
    @auth.login_required
    @etag
    @json
    def index():
        from api.v1 import get_catalog as v1_catalog
        return {'versions': {'v1': v1_catalog()}}

    @app.errorhandler(404)
    @auth.login_required
    def not_found_error(e):
        return not_found('item not found')

    @app.errorhandler(405)
    def method_not_allowed_error(e):
        return not_allowed()

    return app
