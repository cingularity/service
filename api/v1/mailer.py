from flask import request, g, current_app, url_for
from ..models import db, User
from ..decorators import json, collection, etag, admin_required, moderator_required
from . import api
from ..errors import not_allowed, bad_request, not_acceptable
from ..helpers import args_from_url
from ..email import send_contact_form


email_types = ['contactform', 'confirmemail']


@api.route('/sendemail/', methods=['POST'])
def sendEmail():
    data = request.get_json(force=True)
    if not data:
        return bad_request(message='data insufficient')
    if not data['formtype'].lower() in email_types:
        return bad_request(message='specify formtype')
    print('data', data)
    send_contact_form(
        to=data.get('to', "info@ashah.de"),
        sender=data.get('sender', 'Unknown'),
        subject=data.get('subject', 'no subject'),
        body=data.get('body', 'no body'),
        name=data.get('name', 'no name'),
        template='email/contactform'
    )
    return {}, 200
