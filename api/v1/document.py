from flask import request, g, url_for
from ..models import db, Document
from ..decorators import json, collection, etag
from . import api
from ..errors import not_allowed, bad_request


@api.route('/document/', methods=['GET'])
@etag
@json
@collection(Document)
def get_documents():
    return Document.query


@api.route('/document/<uuid:id>/', methods=['GET'])
@etag
@json
def get_document(id):
    return Document.query.get_or_404(id)
