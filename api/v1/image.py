import os
import mimetypes
import tempfile
from PIL import Image as pil_img
from io import BytesIO
from uuid import uuid4
from flask import request, g, current_app, url_for
from werkzeug.exceptions import RequestEntityTooLarge
from werkzeug.utils import secure_filename
from ..models import db, User, Image, Document
from ..decorators import json, collection, etag
from . import api
from ..errors import not_allowed, bad_request, not_acceptable
from ..helpers import args_from_url


@api.route('/upload/', methods=['POST'])
@json
def upload():
    try:
        upload_data = request.files
    except RequestEntityTooLarge:
        return ({
            'status': 413,
            'error': 'Request Entity Too Large',
            'message': 'file too large',
        })
    user_ = g.user
    images_added = []
    documents_added = []
    for key, file_ in upload_data.items():
        filetype, ext = file_.content_type.split('/')
        if not ext.lower() in current_app.config['ALLOWED_EXTENSIONS']:
            continue
        new_filename = '{}.{}'.format(str(uuid4()), ext)
        if filetype == 'image':
            file_dir = os.path.join(
                current_app.config['IMAGE_DIR'],
                str(user_.user_id)
            )

            res_dir = os.path.join(
                current_app.config['IMAGE_DIR'],
                'resources'
            )
            directories = [file_dir, res_dir]
            for directory in directories:
                if not os.path.exists(directory):
                    os.makedirs(directory)

            im = pil_img.open(BytesIO(file_.read()))
            thumb_image = im.copy()
            thumb_image.thumbnail((512, 512), pil_img.ANTIALIAS)

            im.save(os.path.join(file_dir, new_filename))
            thumb_image.save(os.path.join(res_dir, new_filename))
            image_data_dict = {
                'name': key,
                'original_filename': file_.filename,
                'full_image': url_for('static',
                                      filename=(os.path.relpath(
                                          file_dir,
                                          current_app.config['STATIC_DIR'])+'/'+new_filename
                                      ), _external=True),
                'thumbnail': url_for('static',
                                     filename=(os.path.relpath(
                                         res_dir, current_app.config['STATIC_DIR']) + '/' + new_filename
                                     ), _external=True),
            }
            images_added.append(image_data_dict)
            # user_.images.append(Image().import_data(image_data_dict))
            continue
        if filetype == 'application':
            document_dir = os.path.join(
                current_app.config['DOCUMENT_DIR'],
                str(user_.user_id)
            )
            directories = [document_dir, res_dir]
            for directory in directories:
                if not os.path.exists(directory):
                    os.makedirs(directory)
            file_.save(os.path.join(document_dir, new_filename))
            document_data_dict = {
                'name': key,
                'original_filename': file_.filename,
                'document': url_for('static',
                                    filename=(os.path.relpath(
                                        document_dir, current_app.config['STATIC_DIR']) + '/' + new_filename),
                                    _external=True
                                    )
            }
            documents_added.append(document_data_dict)
            # user_.documents.append(Document().import_data(document_data_dict))
    u_images = [Image().import_data(image) for image in images_added]
    u_documents = [Document().import_data(document)
                   for document in documents_added]
    user_.images.extend((u_images))
    user_.documents.extend((u_documents))
    db.session.commit()
    return {'images': images_added, 'documents': documents_added}, 200
