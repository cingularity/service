from flask import request, g, current_app, url_for
from ..models import db, User
from ..decorators import json, collection, etag, admin_required, moderator_required
from . import api
from ..errors import not_allowed, bad_request, not_acceptable
from ..helpers import args_from_url


@api.route('/user/', methods=['GET'])
@etag
@json
@collection(User)
def get_users():
    return User.query