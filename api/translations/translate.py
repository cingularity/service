
base = {
    'confirm_email': {
        'de': 'Bitte das Email bestätigen.',
        'en': 'Please confirm the email.'
    },
    'password_reset': {
        'de': 'Password das Passwort zuurücksetzen.',
        'en': 'Reset your password.'
    }
}


def translate(word, language):
    if not base[word][language]:
        return word
    return base[word][language]
