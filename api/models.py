from datetime import datetime
from dateutil.parser import isoparse
from markdown import markdown
import bleach
from werkzeug.security import generate_password_hash, check_password_hash
from werkzeug.exceptions import NotFound
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from flask import url_for, current_app, g, jsonify
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy import text as sa_text
from flask_sqlalchemy import SQLAlchemy
from .helpers import args_from_url
from .errors import ValidationError
from .helper import comma2Point


db = SQLAlchemy()

### for User management ###


class Permission:
    FOLLOW = 1
    COMMENT = 2
    WRITE = 4
    PROJECT = 8
    ADMIN = 16


class Role(db.Model):
    __tablename__ = 'roles'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), unique=True)
    default = db.Column(db.Boolean, default=False, index=True)
    permissions = db.Column(db.Integer)
    users = db.relationship('User', backref='role', lazy='dynamic')

    def __init__(self, **kwargs):
        super(Role, self).__init__(**kwargs)
        if self.permissions is None:
            self.permissions = 0

    @staticmethod
    def insert_roles():
        roles = {
            'User': [Permission.FOLLOW, Permission.COMMENT],
            'Moderator': [Permission.FOLLOW, Permission.COMMENT,
                          Permission.WRITE, Permission.PROJECT],
            'Administrator': [Permission.FOLLOW, Permission.COMMENT,
                              Permission.WRITE, Permission.PROJECT,
                              Permission.ADMIN],
        }
        default_role = 'User'
        for r in roles:
            role = Role.query.filter_by(name=r).first()
            if role is None:
                role = Role(name=r)
            role.reset_permissions()
            for perm in roles[r]:
                role.add_permission(perm)
            role.default = (role.name == default_role)
            db.session.add(role)
        db.session.commit()

    def add_permission(self, perm):
        if not self.has_permission(perm):
            self.permissions += perm

    def remove_permission(self, perm):
        if self.has_permission(perm):
            self.permissions -= perm

    def reset_permissions(self):
        self.permissions = 0

    def has_permission(self, perm):
        return self.permissions & perm == perm

    def __repr__(self):
        return '<Role: %r>' % self.name


class User(db.Model):
    __tablename__ = 'users'
    user_id = db.Column(db.Integer, primary_key=True)
    # Use email as user name.
    username = db.Column(db.String(64), index=True,
                         unique=True, nullable=False)
    password_hash = db.Column(db.String(128), nullable=False)
    role_id = db.Column(db.Integer, db.ForeignKey('roles.id'))
    confirmed = db.Column(db.Boolean, default=False)
    name = db.Column(db.String(64), index=True)
    profile_image = db.Column(db.String(128))
    member_since = db.Column(db.DateTime, default=datetime.utcnow)
    last_seen = db.Column(
        db.DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)

    def __init__(self, **kwargs):
        super(User, self).__init__(**kwargs)
        if self.role is None:
            if self.username == current_app.config['SITE_ADMIN']:
                self.role = Role.query.filter_by(name='Administrator').first()
            if self.role is None:
                self.role = Role.query.filter_by(default=True).first()

    def get_url(self):
        return url_for('api.get_users', id=self.user_id, _external=True)

    @property
    def password(self):
        raise AttributeError('password is not a readable attribute')

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)

    def generate_auth_token(self, expires_in=None):
        s = Serializer(current_app.config['SECRET_KEY'], expires_in=expires_in)
        return s.dumps({'id': self.user_id}).decode('utf-8')

    def generate_reset_token(self, expiration=3600):
        s = Serializer(current_app.config['SECRET_KEY'], expiration)
        return s.dumps({'reset': self.user_id}).decode('utf-8')

    def generate_confirmation_token(self, expiration=3600):
        s = Serializer(current_app.config['SECRET_KEY'], expiration)
        return s.dumps({'confirm': self.user_id}).decode('utf-8')

    @staticmethod
    def confirm(token):
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            data = s.loads(token.encode('utf-8'))
        except:
            return False
        user = User.query.get(data.get('confirm'))
        if user is None:
            return None
        user.confirmed = True
        db.session.add(user)
        return True

    @staticmethod
    def reset_password(token, new_password):
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            data = s.loads(token.encode('utf-8'))
        except:
            return False
        user = User.query.get(data.get('reset'))
        if user is None:
            return None
        user.password = new_password
        db.session.add(user)
        return True

    @staticmethod
    def verify_auth_token(token):
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except:
            return None
        return User.query.get(data.get('id', None))

    def export_data(self):
        return {
            'self_url': self.get_url(),
            'id': self.user_id,
            'username': self.username,
            'name': self.name,
            'confirmed': self.confirmed,
            'member_since': self.member_since.isoformat() + 'Z',
            'last_seen': self.last_seen.isoformat() + 'Z',
            'language': self.language.value,
            'role': self.role.name
        }

    def import_data(self, data):
        try:
            self.username = data['username']
            self.name = data['name']
        except KeyError as e:
            raise ValidationError('Invalid user: missing ' + e.args[0])
        return self

    def can(self, perm):
        return self.role is not None and self.role.has_permission(perm)

    def is_administrator(self):
        return self.can(Permission.ADMIN)

    def is_moderator(self):
        return self.can(Permission.PROJECT)

    def __repr__(self):
        return '<Username: "{self.username}", confirmed: "{self.confirmed}", Role: "{self.role}", name: "{self.name}" >'.format(self=self)

### End for user Management ###
# Basic Data


class Image(db.Model):
    __tablename__ = 'images'

    _id = db.Column(UUID(as_uuid=True), primary_key=True,
                    server_default=sa_text("uuid_generate_v4()"),)
    name = db.Column(db.String(64))
    thumbnail = db.Column(db.String(256))
    full_image = db.Column(db.String(256))
    original_filename = db.Column(db.String(256))

    user_id = db.Column(db.Integer, db.ForeignKey('users.user_id'))
    user = db.relationship('User', backref=db.backref('images'))

    def get_url(self):
        return url_for('api.get_image', id=self._id, _external=True)

    def export_data(self):
        return {
            'self_url': self.get_url(),
            'name': self.name,
            'thumbnail': self.thumbnail,
            'full_image': self.full_image, 
            'original_filename': self.original_filename,
        }

    def import_data(self, data):
        try:
            self.name=data['name']
            self.thumbnail=data['thumbnail']
            self.full_image=data['full_image']
            self.original_filename=data['original_filename']
        except KeyError as e:
            raise ValidationError('missing:', e.args[0])
        return self

    def __repr__(self):
        return "<Image: name='{self.name}', thumbnail='{self.thumbnail}'' >".format(self=self)


class Document(db.Model):
    __tablename__ = 'documents'

    _id = db.Column(UUID(as_uuid=True), primary_key=True,
                    server_default=sa_text("uuid_generate_v4()"),)
    name = db.Column(db.String(64))
    document = db.Column(db.String(256))
    original_filename = db.Column(db.String(256))
    user_id = db.Column(db.Integer, db.ForeignKey('users.user_id'))
    user = db.relationship('User', backref=db.backref('documents'))

    def get_url(self):
        return url_for('api.get_document', id=self._id, _external=True)

    def export_data(self):
        return {
            'self_url': self.get_url(),
            'name': self.name,
            'document': self.document,
            'original_filename': self.original_filename
        }

    def import_data(self, data):
        try:
            self.name=data['name']
            self.document=data['document']
            self.original_filename=data['original_filename']
        except KeyError as e:
            raise ValidationError('missing:', e.args[0])
        return self

    def __repr__(self):
        return "<Document: name='{self.name}', document='{self.document}' >".format(self=self)