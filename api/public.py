import os
from scipy import spatial
from datetime import datetime
from flask_cors import CORS
from flask import Blueprint, jsonify, g, request, current_app, url_for
from validate_email import validate_email
from .models import User, db
from .errors import unauthorized, bad_request, not_allowed, not_found
from .decorators import json, collection, etag
from .email import send_email
from .helpers import args_from_url


public = Blueprint('public', __name__)


@public.route('/', methods=['GET'])
def index():
    return {'versions': {'v1': v1_catalog()}}


def v1_catalog():
    return {
        'register': url_for('public.register', _external=True),
        'contact': url_for('public.contact', _external=True),
        'new_confirmation': url_for('public.new_confirmation', _external=True),
        'register_confirmation': url_for('public.register_confirmation', _external=True),
        'reset_password_request': url_for('public.reset_password_request', _external=True),
        'reset_password': url_for('public.reset_password', _external=True)
    }


@public.route('/register/', methods=['POST'])
def register():
    '''
    Use to register new user. It also send any email with the confirmation token
        POST request taken 'username', 'password'   # validate username to be an email.
    '''
    data = request.get_json(force=True)
    try:
        username = data['username']
    except KeyError as e:
        return bad_request('error_missing_username' + e.args[0])

    if not validate_email(username):
        return bad_request('not_a_valid_email')

    try:
        password = data['password']
    except:
        return bad_request('password_required')

    if password is "":
        return bad_request('password_is_blank')

    # email already taken
    if User.query.filter_by(username=username).first():
        return bad_request('email_aready_used')

    user_ = User(username=username, password=password)
    db.session.add(user_)
    db.session.commit()
    token = user_.generate_confirmation_token()
    send_email(user_.username, 'confirm_email',
               'auth/email/confirm', user=user_, token=token, language=user_.language)
    return {}, 201, {'Location': '/auth/confirm'}


@public.route('/contact/', methods=['POST'])
@json
def contact():
    '''
        This will get the submitted contact form
    '''
    data = request.get_json(force=True)
    send_email(os.environ.get['SITE_ADMIN'], data['subject'],
               'contact/email/contact', name=data['name'],
               contact_email=data['email'], body=data['body']
               )
    return {'message': 'thank_you'}, 200


@public.route('/new-confirmation/', methods=['POST'])
def new_confirmation():
    '''
    POST should have 
        username
    '''
    data = request.get_json(force=True)
    try:
        user = User.query.filter_by(username=data['username']).first()
    except:
        return bad_request('email_already_used')
    token = user.generate_confirmation_token()
    send_email(user.username, 'confirm_email',
               'auth/email/confirm', user=user, token=token, language=user.language)
    return {}, 200, {'Location': '/auth/confirm'}


@public.route('/register-confirmation/', methods=['POST'])
def register_confirmation():
    '''
    POST should have 
        token
    '''
    data = request.get_json(force=True)
    if User.confirm(data['token']):
        db.session.commit()
        return {}, 201, {'Location': '/auth/request-token'}
    else:
        return bad_request('invalid_username')


@public.route('/reset/', methods=['POST'])
def reset_password_request():
    '''
    POST should contain
        username
    How does it work? when the user sends a request, he will receive a token
    With it the user can reset the password
    '''
    data = request.get_json(force=True)
    try:
        user = User.query.filter_by(username=data['username']).first()
    except:
        return bad_request('invalid_username')
    if user is None:
        return bad_request('username_not_given')
    token = user.generate_reset_token()
    send_email(user.username, 'password_reset',
               'auth/email/reset_password', user=user, token=token, language=user.language)
    return {}, 200, {'Location': '/public/reset-password'}


@public.route('/reset-password/', methods=['POST'])
def reset_password():
    '''
    POST should have 
        token, new_password
    '''
    data = request.get_json(force=True)
    if User.reset_password(data['token'], data['new_password']):
        db.session.commit()
        return {}, 201, {'Location': '/auth/request-token'}
    else:
        return bad_request('password_reset_failed')