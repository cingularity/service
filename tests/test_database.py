import unittest
from werkzeug.exceptions import BadRequest
from tests.test_client import RegClient
from api.app import create_app
from api.models import db, User, Role, Graveyard
from api.errors import ValidationError


class TestDatabase(unittest.TestCase):
    default_username = 'mylifenp@yahoo.com'
    default_password = 'cat'

    admin_username = 'admin@ashah.de'
    admin_password = 'cat'

    moderator_username = 'moderator@ashah.de'
    moderator_password = 'cat'

    address = {
        'street': 'Rebenstr,',
        'house_number': '4',
        'zip_number': '76227',
        'city': 'Karlsruhe',
        'country': 'Germany'
    }

    def setUp(self):
        self.app = create_app('test_config')
        self.ctx = self.app.app_context()
        self.ctx.push()
        db.drop_all()
        db.create_all()
        Role.insert_roles()
        u_ad = User(
            username=self.admin_username,
            password=self.admin_password,
            role=Role.query.filter_by(name='Administrator').first()
        )
        db.session.add(u_ad)
        db.session.commit()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.ctx.pop()

    def test_graveyard_creation(self):
        # create graveyard
        data = {
            "moderator": {
                "username": self.moderator_username,
                "password": self.moderator_password,
            },
            "address": self.address,
            "name": 'Bergfriedhof Durlach'
        }
        graveyard = Graveyard().import_data(data)
        db.session.add(graveyard)
        db.session.commit()
        gr1 = Graveyard.query.all()[0]
        # the name
        self.assertEqual(gr1.name, 'Bergfriedhof Durlach')
        #the address
        self.assertEqual(gr1.address.zip_number, '76227')

