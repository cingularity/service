# import unittest
# from werkzeug.exceptions import BadRequest
# from tests.test_client import RegClient
# from api.app import create_app
# from api.models import db, User, Role
# from api.errors import ValidationError

# """
# All Userrelated test. Buyers are the ones that are offering services


# """

# class BuyerAPI(unittest.TestCase):
#     default_username = 'amit@ashah.de'
#     default_password = 'cat'
#     admin_username = 'info@ashah.de'
#     admin_password= "Dragon123"

#     def setUp(self):
#         self.app = create_app('test_config')
#         self.ctx = self.app.app_context()
#         self.ctx.push()
#         db.drop_all()
#         db.create_all()
#         Role.insert_roles()
#         u = User(username=self.default_username,
#                  password=self.default_password)
#         # a = User(username=self.admin_username,
#         #          password=self.admin_password)
#         # a.role = Role.query.filter_by(name='Administrator').first()
#         #db.session.add_all([p, a])
#         db.session.add(u)
#         db.session.commit()
#         self.client = RegClient(self.app, u.generate_auth_token(), '')
#         #self.admin = RegClient(self.app, a.generate_auth_token(), '')
#         self.catalog = self._get_catalog()

#     def tearDown(self):
#         db.session.remove()
#         db.drop_all()
#         self.ctx.pop()

#     def _get_catalog(self, version='v1'):
#         rv, json = self.client.get('/')
#         return json['versions'][version]

#     """
#     Buyers are the ones that have Role as 'Buyer'

#     """
#     # Non API testing
#     def test_user_role(self):
#         # the buyer is still a normal user
#         user = User.query.filter_by(username=self.default_username).first()
#         self.assertTrue(user.role == Role.query.filter_by(name='User').first())
#         # get the profile
#         rv, json = self.client.get('/v1/users/{}/'.format(user.user_id))
#         self.assertEqual(rv.status_code, 200, msg=json)
#         # getting the address
#         rv, json = self.client.get(json['addresses_url'])
#         self.assertEqual(rv.status_code, 200, msg=json)
#         # adding address
#         address= {
#             "street": "Rebenstr.",
#             "house_number": "5",
#             "zip_number": "76227",
#             "city": "Karlsruhe",
#             "country": "Germany",
#         }
#         rv, json = self.client.post('/v1/users/address/', data=address)
#         self.assertEqual(rv.status_code, 201, msg=json)
#         # updating address
#         address['house_number']='6'
#         rv, json = self.client.post('/v1/users/address/', data=address)
#         self.assertEqual(rv.status_code, 200, msg=json)
#         self.assertTrue(user.addresses[0].house_number == address['house_number'])