import os
import redis


basedir = os.path.abspath(os.path.dirname(__file__))

class Config:
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'hard to guess string'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    USE_TOKEN_AUTH = True
    ERRORS_FILE = os.path.join(basedir, 'errors.txt')
    SITE_LIST = [
        "http://localhost:3000",
        "http://192.168.178.35:3000",
        "http://localhost:3001",
        "https://adfinem.datenium.com",
        "https://www.adfinem.datenium.com",
        "https://serbot.de",
        "https://www.serbot.de",
        "https://adfinemadmin.datenium.com",
        "https://www.adfinemadmin.datenium.com",
    ]

    # Uploads
    LOGFILE = os.path.join(basedir, 'error.log')
    STATIC_DIR = os.path.join(basedir, 'api',  'static/')
    MEDIA_DIR = os.path.join(STATIC_DIR, 'media/')
    ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'bmp', 'pdf'])
    IMAGE_DIR = os.path.join(MEDIA_DIR, 'images/')
    VIDEO_DIR = os.path.join(MEDIA_DIR, 'videos/')
    INVOICE_DIR = os.path.join(MEDIA_DIR, 'invoices/')
    DOCUMENT_DIR = os.path.join(MEDIA_DIR, 'documents/')
    THUMBNAIL_SIZE = (600, 600)

    # the site URL

    SITE_ADMIN = 'info@ashah.de'  # this user automatically becomes the site admin

    # mail
    MAIL_SERVER = os.environ.get('MAIL_SERVER', 'mail1.pwhost.de')
    MAIL_PORT = int(os.environ.get('MAIL_PORT', '587'))
    MAIL_USE_TLS = os.environ.get('MAIL_USE_TLS', 'true').lower() in \
        ['true', 'on', '1']
    MAIL_USERNAME = os.environ.get('MAIL_USERNAME')
    MAIL_PASSWORD = os.environ.get('MAIL_PASSWORD')
    MAIL_SUBJECT_PREFIX = '[Service]'
    MAIL_SENDER = 'Service Admin <info@ashah.de>'

    # Compression
    COMPRESS_MIMETYPES = ['text/html', 'text/css', 'text/xml', 'application/json', 'application/javascript']
    COMPRESS_LEVEL = 6
    COMPRESS_MIN_SIZE = 500
    USE_REDIS=False

    # enable rate limits only if redis is running
    USE_RATE_LIMITS = False
    if USE_REDIS:
        try:
            r = redis.Redis()
            r.ping()
            USE_RATE_LIMITS = True
        except redis.ConnectionError:
            USE_RATE_LIMITS = False


class DevelopmentConfig(Config):
    DEBUG = True
    SITE_URL = 'http://localhost:5000/'
    # development database
    SQLALCHEMY_DATABASE_URI = 'postgresql+psycopg2://service:Service123@localhost/service'


class TestingConfig(Config):
    TESTING = True
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'dev.sqlite')
    USE_RATE_LIMITS = False
    WTF_CSRF_ENABLED = False


class ProductionConfig(Config):
    SITE_URL = 'https//www.service.cingularity.de/'
    SQLALCHEMY_DATABASE_URI = os.environ.get('PRO_DATABASE_URL')


config = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'production': ProductionConfig,

    'default': DevelopmentConfig,
}
